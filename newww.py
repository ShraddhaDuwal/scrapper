import requests as req
from bs4 import BeautifulSoup as bs
import json

url = req.get('http://www.nepalstock.com/marketdepthofcompany/132')
soup = bs(url.content, "html.parser")

index = soup.find("table", {"class": "depthIndex"})
tables = soup.find_all("table", {"class": "orderTable"})

dic, li, l = [], {}, {}
def is_num(x):
	try:
		float(x)
		return True
	except ValueError:
		return False

try:
	for index_data in index.find_all("td"):
		entry = index_data.get_text().split()

		if len(entry) > 2:
			if is_num(entry[0]):
				li = {'main':[entry[0], entry[1], entry[2]]}
			if not is_num(entry[1]):
				entry[0] = entry[0] + entry[1]
				entry[1] = entry[2]
				del entry[2]

		dic.append(entry)
		l = dict(dic[1:])

	a = dict(list(li.items()) + list(l.items()))
	print(a)

	print("-------------")
	titles, value, total, t = [], [], [], {}

	for table in tables: 
		for item in table.find_all("th"):
			titles.append(item.get_text())
		for i in table.find_all("tr"):
			value.append([j.get_text() for j in i.find_all("td")])

	excludedvalues = value.pop()

	buy, sell = value[:len(value)//2], value[len(value)//2:]
	f_table = []

	for i in zip(buy, sell):
		f_table.append(i[0]+i[1])

	f_table.pop(0)

	list_of_dict = []

	for i in range(len(f_table)):
		entry = {}
		for j in range(len(titles)):
			entry[titles[j]] = f_table[i][j]
		list_of_dict.append(entry)

	h = json.dumps(list_of_dict)
	print(h)

	print("----------")
	t = {"result": excludedvalues}
	t = json.dumps(t)
	print(t)
except IndexError:
	print("No data online")
